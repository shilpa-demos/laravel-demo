@extends('layouts.master')

@section('content')
<section class="blog-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-7 order-md-1 order-1">
                <div class="blog__details__text">
                    <h3>{{$blog->title}}</h3>
                    <img src="{{asset('/storage/'.$blog->photo_src)}}" />
                    <p> {{$blog->description}}</p>
                </div>
                <div class="blog__details__content">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="blog__details__author">
                                <div class="blog__details__author__text">
                                    <h6>{{$blog->user->name}}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="blog__details__widget">
                                <ul>
                                    <li><span>Categories:</span> {{$blog->category->title}}</li>
                                    <li><span>Tags:</span> {{ str_replace(['"', '[', ']'], [' ', '', ''], $blog->tags->pluck('name'))}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                @if (session('notification'))
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissible fade show">
                        <strong>Success!</strong> {{ session('notification') }}
                    </div>
                @endif

                @if (Route::has('login'))
                    @auth
                    <form action="{{ route('comment.store', [$blog->id]) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <h5>Comment</h5>
                            <textarea class="form-control" name="content" rows="10"></textarea>
                            <input value="{{Auth::user()->id}}" type="hidden" name="user_id">
                            <input value="{{$blog->id}}" type="hidden" name="blog_id">
                            @if($errors->has('content'))
                                <div class="alert alert-danger" role="alert">{{ $errors->first('content') }}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-primary">Submit</button>
                        </div>
                    </form>
                    @endauth
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Content</th>
                        <th scope="col">User</th>
                        <th scope="col">Date</th>
                        </tr>
                    </thead>
                @foreach ($comments as $item)
                    <tbody>
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{$item->content}}</td>
                            <td>{{$item->user->name}}</td>
                            <td>{{$item->created_at->format("M d, Y")}}</td>
                        </tr>
                    </tbody>
                @endforeach
                @if($comments->isEmpty())
                <tr>
                    <td colspan="10">
                        <div class="flex flex-col justify-center items-center py-4 text-lg">
                            {{ 'No Result Found' }}
                        </div>
                    </td>
                </tr>
                @endif
                </table>
            </div>
        </div>
    </div>
</section>
@endsection
