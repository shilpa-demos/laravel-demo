@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
    @if ($blogs)
        @foreach ($blogs as $item)
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="blog__item">
                <div class="blog__item__pic">
                    <img class="card-img-top" src="{{asset('/storage/'.$item->photo_src)}}" />
                </div>
                <div class="blog__item__text">
                    <ul>
                        <li><i class="fa fa-calendar-o"></i> {{$item->created_at->format("M d, Y")}}</li>
                        <li><i class="fa fa-comment-o"></i> {{$item->comments_count}}</li>
                    </ul>
                    <h5><a href="{{route('blogdetail', $item->id)}}">{{$item->title}}</a></h5>
                    <p>{{Str::limit($item->description, 200)}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif
    </div>
</div>
@endsection
