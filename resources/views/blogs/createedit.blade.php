<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Blog Data') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div class="container">
                        {{-- @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif --}}
                        <div class="row">
                            <div class="col-12">
                                <h1 class="mt-3 mb-3 float-left"><b>Blog {{$action}}</b></h1>
                                <a class="mt-3 mb-3 float-right btn btn-primary" href="{{ route('blog.index')}}" role="button">Back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <form action="{{ ($action=="Add") ? route('blog.store') : route('blog.update', [$blog->id]) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @if ($action=='Update')
                                        @method('patch')
                                    @endif
                                    <div class="form-group">
                                        <label for="">Title</label>
                                        <input value="{{isset($blog->title) ? $blog->title : ''}}" type="text" class="form-control" name="title" placeholder="Blog Title">
                                        <input value="{{Auth::user()->id}}" type="hidden" name="user_id">
                                    </div>
                                    @if($errors->has('title'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('title') }}</div>
                                    @endif
                                    <div class="form-group">
                                        <label for="">Description</label>
                                        <textarea class="form-control" name="description" rows="3">{{isset($blog->description) ? $blog->description : ''}}</textarea>
                                    </div>
                                    @if($errors->has('description'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('description') }}</div>
                                    @endif
                                    <div class="form-group">
                                        <label for="">Blog Photo</label>
                                        <input type="file" class="form-control-file" name="photo_src">
                                        @if (isset($blog->photo_src))
                                            <img class="mt-3" width="20%" src="{{asset('/storage/'.$blog->photo_src)}}" />
                                        @else
                                        @endif
                                    </div>
                                    @if($errors->has('photo_src'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('photo_src') }}</div>
                                    @endif
                                    <div class="form-group">
                                        <label for="">Category</label>
                                        <select class="form-control" name="category_id">
                                            <option value="">Select Category</option>
                                            @foreach ($categories as $item)
                                            <option {{(isset($blog->category_id) && $blog->category_id==$item->id) ? 'selected' : ''}} value="{{$item->id}}">{{$item->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($errors->has('category_id'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('category_id') }}</div>
                                    @endif
                                    <div class="form-group">
                                        <label for="">Tag</label>
                                        <select multiple class="form-control" name="tag_id[]">
                                            @foreach ($tags as $item)
                                            <option {{(isset($blog->tags) ? (in_array($item->id, $blog->tags->pluck('pivot')->pluck('tag_id')->toArray()) ? "selected": "") : '')}} value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($errors->has('tag_id'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('tag_id') }}</div>
                                    @endif
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

