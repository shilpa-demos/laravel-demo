<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Blog Data') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                @if (session('notification'))
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissible fade show">
                                        <strong>Success!</strong> {{ session('notification') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                                    </div>
                                @endif
                                @if (session('delnotification'))
                                    <!-- Error Alert -->
                                    <div class="alert alert-danger alert-dismissible fade show">
                                        <strong>Delete!</strong> {{ session('delnotification') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                                    </div>
                                @endif
                                <h1 class="mt-3 mb-3 float-left"><b>Blog Listing</b></h1>
                                <a class="mt-3 mb-3 float-right btn btn-primary" href="{{ route('blog.create')}}" role="button">Create</a>
                                @if ($blogs)
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Photo</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Category</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Action</th>
                                        </tr>
                                    </thead>

                                    @foreach ($blogs as $item)
                                        <tbody>
                                            <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td width="15%">
                                                <img src="{{asset('/storage/'.$item->photo_src)}}" />
                                            </td>
                                            <td>{{$item->title}}</td>
                                            <td>{{$item->category->title}}</td>
                                            <td>{{$item->user->name}}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('blog.show', $item)}}" role="button">Show</a>

                                                <a class="btn btn-info" href="{{ route('blog.edit', $item->id)}}" role="button">Edit</a>

                                                <form action="{{ route('blog.destroy', $item->id)}}" method="POST">
                                                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                                                    @csrf @method('DELETE')
                                                </form>
                                            </td>
                                            </tr>
                                        </tbody>
                                    @endforeach

                                    {{$blogs->links()}}

                                    @if($blogs->isEmpty())
                                    <tr>
                                        <td colspan="10">
                                            <div class="flex flex-col justify-center items-center py-4 text-lg">
                                                {{ __('No Result Found') }}
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

