<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Blog Data') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="mt-3 mb-3 float-left"><b>Blog Add</b></h1>
                                <a class="mt-3 mb-3 float-right btn btn-primary" href="{{ route('blog.index')}}" role="button">Back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <strong>Title: </strong>
                                    {{$blog->title}}
                                </div>

                                <div class="form-group">
                                    <strong>Description: </strong>
                                    {{$blog->description}}
                                </div>

                                <div class="form-group">
                                    <strong>Blog Photo: </strong>
                                    <img width="20%" src="{{asset('/storage/'.$blog->photo_src)}}" />
                                </div>

                                <div class="form-group">
                                    <strong>Category: </strong>
                                    {{$blog->category->title}}
                                </div>

                                <div class="form-group">
                                    <strong>Tag: </strong>
                                    {{ str_replace(['"', '[', ']'], [' ', '', ''], $blog->tags->pluck('name'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

