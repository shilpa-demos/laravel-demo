<!-- Footer Section Begin -->
<footer class="footer">
    <div class="col-lg-12">
        <div class="footer__copyright">
            <div class="footer__copyright__text">
                <p>
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved.
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->
