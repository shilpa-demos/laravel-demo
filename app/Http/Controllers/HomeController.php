<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comment;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function index(Request $request)
    {
        //$blogs = Blog::all();
        $blogs = Blog::withCount('comments')->get();

        return view('welcome', compact('blogs'));
    }
    /**
     * Blog Detail view generate
     */
    public function blogdetail ($id) {
        $blog = Blog::find($id);
        $comments = Comment::where('blog_id', '=', $id)->get();

        return view('blog-detail', compact('blog', 'comments'));
    }
}
