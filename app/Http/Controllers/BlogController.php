<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogTags;
use App\Models\Category;
use App\Models\Tag;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //$blogs = Blog::all();
        $blogs = Blog::paginate(2);
        return view('blogs.index', compact('blogs'));
    }

    /**
     *  creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();

        /*
        if (count($categories)==0){
            $cate = Category::create([
                'title' => 'Food',
            ]);

            $cate = Category::create([
                'title' => 'Beauty',
            ]);

            $cate = Category::create([
                'title' => 'Test 1',
            ]);

            $cate = Category::create([
                'title' => 'Test 2',
            ]);

            $categories = Category::all();
        }

        if (count($tags)==0){
            $cate = Tag::create([
                'name' => 'Trending',
            ]);

            $cate = Tag::create([
                'name' => 'Life Style',
            ]);

            $cate = Tag::create([
                'name' => 'Healthy Food',
            ]);

            $cate = Tag::create([
                'name' => 'Cooking',
            ]);

            $tags = Tag::all();
        }
        */

        $blog = [];
        $action = "Add";
        return view('blogs.createedit', compact('blog','categories', 'tags', 'action'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // add validation
        $request->validate([
            'title' => 'required|max:100',
            'description' => 'required',
            'photo_src' => ['required', 'min:10', 'max:5000', 'mimes:png,jpg,gif'],
            'category_id' => 'required',
            //'tag_id' => 'required',
        ]);

        $fileName = time().'_'.$request->photo_src->getClientOriginalName();
        $data = $request->except('photo_src');
        $data['photo_src'] = $request->photo_src->storeAs('/blog', $fileName);

        $blog = Blog::create($data);
        $blog->tags()->attach($request->tag_id);
        return redirect(route('blog.index'))->with('notification', 'New Blog created successfully.!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog)
    {
        return view('blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog)
    {
        $action = "Update";
        $categories = Category::all();
        $tags = Tag::all();


        return view('blogs.createedit', compact('blog', 'categories', 'tags', 'action'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Blog $blog)
    {
        // add validation
        if (isset($request->photo_src)) {
            $request->validate([
                'title' => 'required|max:100',
                'description' => 'required',
                'photo_src' => ['required', 'min:10', 'max:5000', 'mimes:png,jpg,gif'],
                'category_id' => 'required',
                //'tag_id' => 'required',
            ]);
        } else {
            $request->validate([
                'title' => 'required|max:100',
                'description' => 'required',
                'category_id' => 'required',
                //'tag_id' => 'required',
            ]);
        }

        $data = $request->except('photo_src');

        if (isset($request->photo_src)) {
            $fileName = time().'_'.$request->photo_src->getClientOriginalName();
            $data['photo_src'] = $request->photo_src->storeAs('/blog', $fileName);

            Storage::disk('public')->delete([$blog->photo_src]);
        }

        BlogTags::where('blog_id', '=', $blog->id)->delete();

        $blog->update($data);
        $blog->tags()->attach($request->tag_id);

        return redirect(route('blog.index'))->with('notification', 'Blog updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();
        return redirect(route('blog.index'))->with('delnotification', 'Blog Title ::-' . $blog->title .  ' deleted successfully!');
    }
}
