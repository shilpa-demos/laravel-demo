<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Blog;
use App\Models\Tag;

class BlogTags extends Model
{
    use HasFactory;
    protected $fillable = [
        'blog_id',
        'tag_id',
    ];

    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
